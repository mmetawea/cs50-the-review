using CSV

# open CSV
file = "title.basics.tsv"
df = CSV.read(file, delim = "\t", limit = 10)


# manually construct a `CSV.Source` once, then stream its data to both a DataFrame
# and SQLite table `sqlite_table` in the SQLite database `db`
# note the use of `CSV.reset!` to ensure the `source` can be streamed from again
# source = CSV.Source(file)
# df1 = CSV.read(source, DataFrame)
# CSV.reset!(source)


open(file) do filehandle
    for line in eachline(filehandle)
        row = CSV.read(line, delim = '\t')
        println(line)
        println(row)
    end
end