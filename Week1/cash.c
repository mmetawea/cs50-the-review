/*
According to the National Institute of Standards and Technology (NIST), a greedy algorithm 
is one “that always takes the best immediate, or local, solution while finding an answer. 

Greedy algorithms find the overall, or globally, optimal solution for some optimization problems, 
but may find less-than-optimal solutions for some instances of other problems.”

Implement, in cash.c at right, a program that first asks the user how much change 
is owed and then prints the minimum number of coins with which that change can be made.
*/

#include <stdio.h>

int main(void)
{

    int penny = 1;
    int nickel = 5;
    int dime = 10;
    int quarter = 25;

    int change;
    printf("Please enter the change: ");
    scanf("%d", &change);

    // int change = 25;
    int counter = 0;
    do
    {

        if (change >= quarter)
        {
            change = change - quarter;
            // printf("%i\n", change);
            printf("Quarter\n");
            counter++;
        }

        if (change >= dime && change < quarter)
        {
            change = change - dime;
            // printf("%i\n", change);
            printf("Dime\n");
            counter++;
        }

        if (change >= nickel && change < quarter && change < dime)
        {
            change = change - nickel;
            // printf("%i\n", change);
            printf("Nickel\n");
            counter++;
        }

        if (change > 0 && change < quarter && change < dime && change < nickel)
        {
            change = change - penny;
            // printf("%i\n", change);
            printf("Penny\n");
            counter++;
        }

    } while (change > 1);

    printf("Total coins: %i\n", counter);
    return 0;
}
