#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

/*
Specification

Design and implement a program, readability, that computes the Coleman-Liau index of the text.

    Implement your program in a file called readability.c in a directory called readability.
    Your program must prompt the user for a string of text (using get_string).
    Your program should count the number of letters, words, and sentences in the text. You may assume that a letter is any lowercase character from a to z or any uppercase character from A to Z, any sequence of characters separated by spaces should count as a word, and that any occurrence of a period, exclamation point, or question mark indicates the end of a sentence.
    Your program should print as output "Grade X" where X is the grade level computed by the Coleman-Liau formula, rounded to the nearest integer.
    If the resulting index number is 16 or higher (equivalent to or greater than a senior undergraduate reading level), your program should output "Grade 16+" instead of giving the exact index number. If the index number is less than 1, your program should output "Before Grade 1".


*/

float cli( int letter, int word, int sentence);


int main(void)
{
    int letter = 0;
    int word = 0;
    int sentence = 0;

    // prompt user to enter string
    string article = get_string("What is your article?: ");

    // get the length of the article
    int n = strlen(article);

    for (int i = 0; i < n; i++ )
    {
        // get the number of Letters
        if (isalnum(article[i]))
        {
            letter ++;

        }

        // get the number of words
        if (i < n-1 && isspace(article[i] && isalnum( article[i+1])))
        {
            word ++;
        }

        // count sentences
        if (i > 0 && isalnum( article[i-1] && ( article[i] == '!' || article[i] =='?' || article[i] == '.' ) ))
        {
            sentence ++;
        }

    }

    printf("Letters: %i\n Words: %i\n Sentences: %i\n", letter, word, sentence);

    int grade  = cli( letter, word, sentence);
    printf("Coleman-Liau index: %i", grade);
}

float cli( int letter, int word, int sentence)
{
    // CLI = 0.588 * L - 0.296 * S - 15.8
    // L is the average number of letters per 100 words & S is the average number of senteces per 100 words.

    int grade = 0.588 * (100 * letter / word)  - 0.296 * (100 * sentence / word ) - 15.8 ;
    return grade;


}
