# linear search

'''
# Linear Search

    This is the simplest searching technique.
    It sequentially checks each element of the list for the target searching value until a match is found or until all the elements have been searched.
    This searching technique can be performed on both type of list, either the list is sorted or unsorted.

 ## The Ο and Ω notations do only describe the bounds of a function that describes the asymptotic behavior of the actual behavior of the algorithm. Here’s a good

    (Ω) describes the lower bound: f(n) ∈ Ω(g(n)) means the asymptotic behavior of f(n) is not less than g(n)·k for some positive k, so f(n) is always at least as much as g(n)·k.
    (Ο) describes the upper bound: f(n) ∈ Ο(g(n)) means the asymptotic behavior of f(n) is not more than g(n)·k for some positive k, so f(n) is always at most as much as g(n)·k.

Cost - Upper bound time :           O(n)
Best case :                         𝛀(1)
'''


def linear_search(myList, item):
    for i in range(len(myList)):
        if myList[i] == item:
            return i + 1
    return -1


myList = [1, 7, 6, 5, 8]
print("Elements in list : ", myList)

x = int(input("enter searching element :"))
result = linear_search(myList, x)

linear_search(myList, 9)

if result == -1:
    print("Element not found in the list.")
else:
    print("Element " + str(x) + " is found in position %d" % result)
