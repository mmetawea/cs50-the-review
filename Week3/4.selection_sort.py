"""
Selection Sort.
--------------

The selection sort algorithm sorts an array by repeatedly
finding the minimum element (considering ascending order)
from unsorted part and putting it at the beginning.
The algorithm maintains two subarrays in a given array.
1) The subarray which is already sorted.
2) Remaining subarray which is unsorted.

In every iteration of selection sort, the minimum
element (considering ascending order) from the unsorted
subarray is picked and moved to the sorted subarray.
"""

# import math
# if arr[i] < arr[n - 1]:


def selectSort(arr):
    n = len(arr)

    for i in range(n):
        min_index = i
        for j in range(i + 1, n):
            if arr[min_index] > arr[j]:
                min_index = j
        arr[min_index], arr[i] = arr[i], arr[min_index]
    return arr


myList = [100, 20, 80, 30, 10, 50, 60, 40, 70, 90]
print("Elements before sorting : ", myList)

sorted = selectSort(myList)
print("Sorted list : ", sorted)

# print("Elements after sorting : ", sorted)
