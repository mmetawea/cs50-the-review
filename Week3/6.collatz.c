#include "stdio.h"

int collatz(int n);

void main(void)
{
  printf("Collatz for 3 is: %d\n", collatz(3));

  printf("Collatz for 5 is: %d\n", collatz(5));

  printf("Collatz for 27 is: %i\n", collatz(27));
}

int collatz(int n)
{
  // base case
  if (n ==1)
  {
    return 0;
  }
  // even numbers
  else if ((n%2) == 0)
  {
    return 1 + collatz(n/2);
  }
  // odd numbers
  else
    {
      return 1 + collatz(3*n + 1);
    }
}
