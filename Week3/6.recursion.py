def fact(n):
    if n == 1:
        return 1
    else:
        return n * fact(n - 1)


def fact2(n):
    product = 1
    while n > 0:
        product *= n
        n -= 1
    return product


fact(5) == fact2(5)

'''
An example of multiple base cases:
Fibonacci number sequence
- The first element is 0
- The Second element is 1
- The nth number is the sum of (n-1) & (n-2)

An example of multiple recursive cases:
The Collatz conjecture

applied to positive integers and speculates
that it is always possible to get "back to 1" if you follow
these steps:
- if n is 1, stop
- Otherwise, if n is even, repeat this process on n/2
- Otherwise, if n is odd, repeat this process on 3n+1
'''


def gen_seq(length):
    if length <= 1:
        return length
    else:
        return (gen_seq(length - 1) + gen_seq(length - 2))


length = int(input("Enter number of terms: "))

print("Fibonacci sequence using Recursion :")
for iter in range(length):
    print(gen_seq(iter))


def fib():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


for index, fibonacci_number in zip(range(10), fib()):
    print('{i:3}: {f:3}'.format(i=index, f=fibonacci_number))


# Memoized recursion for efficiency
def mem_fib(n, _cache={}):
    '''efficiently memoized recursive function, returns a Fibonacci number'''
    if n in _cache:
        return _cache[n]
    elif n > 1:
        return _cache.setdefault(n, mem_fib(n - 1) + mem_fib(n - 2))
    return n


for i in range(40):
    print(i, mem_fib(i))
