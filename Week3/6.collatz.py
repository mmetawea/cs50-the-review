'''
The Collatz conjecture

applied to positive integers and speculates
that it is always possible to get "back to 1" if you follow
these steps:
- if n is 1, stop
- Otherwise, if n is even, repeat this process on n/2
- Otherwise, if n is odd, repeat this process on 3n+1
'''


def collatz(n):
    if n == 1:
        return 0
    elif n % 2 == 0:
        return collatz(n / 2) + 1
    else:
        return collatz(3 * n + 1) + 1


collatz(5)

collatz(15)

collatz(50)
