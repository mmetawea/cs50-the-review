#include <stdio.h>

int main(void)
{
    char *s = "EMMA";
    printf("%p\n", s);
    printf("%c\n", *s);
    printf("%c\n", *(s+1));
    printf("%s\n", s);
    printf("%p\n", &s[0]);
    printf("%p\n", &s);
    printf("%s\n", "test");
    // printf("%p\n", s);
    const char *h = "hello world";
    printf("%c\n", h[4]); /* outputs an 'o' character */
    printf("%s\n", h);
}