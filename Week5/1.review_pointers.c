/*Lecture 5 - review
A great example to explain variables vs memory pointers and dereferencing.

Note: printf arguemtns for values:
%i intger
%p memory porinter
*/

#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    int *x;
    int *y;
    int z;

    // x = malloc(sizeof(int));

    *x = 42;
    y=x;
    *y = 13;
    z = 3;
    printf("x: %i, *x: %p, y: %i, *y: %p, z: %i, *z: %p\n", x,*x,y, *y, z, z);
}
